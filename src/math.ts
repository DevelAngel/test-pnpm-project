/**
 * Calculates a - b.
 * If the result is negative, it returns 0.
 */
export function saturating_sub(a: number, b: number): number {
  const res = a - b;
  if (res >= 0) {
    return res;
  } else {
    return 0;
  }
}
