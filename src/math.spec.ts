import test, { ExecutionContext } from 'ava';

import { saturating_sub } from './math';

const fn = () => 'foo';

const saturating_sub_test = (t: ExecutionContext, a: number, b: number, expected: number) => {
	t.is(saturating_sub(a, b), expected);
};

test('saturating_sub(3, 1) = 2', saturating_sub_test, 3, 1, 2);
test('saturating_sub(2, 1) = 1', saturating_sub_test, 2, 1, 1);
test('saturating_sub(1, 1) = 0', saturating_sub_test, 1, 1, 0);
test('saturating_sub(0, 1) = 0', saturating_sub_test, 0, 1, 0);
test('saturating_sub(-1, 1) = 0', saturating_sub_test, -1, 1, 0);
test('saturating_sub(-2, -3) = 1', saturating_sub_test, -2, -3, 1);
